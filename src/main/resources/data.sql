INSERT INTO parents (username,password, role) VALUES ('user1','$2a$10$4y4AWKpbwfvKDTm3b69s8epMEj1WwIPSycbu2IfBxRjfSz6uY5kOS', 'USER' );
INSERT INTO parents (username,password, role) VALUES ('user2','$2a$10$hlAJ17is6LNX1WZiWzTRqe3XpDHgB8K7sqMtsTKsKmWQDn0ps7JVy', 'ADMIN');

INSERT INTO setting (key, isrevealed, value, parent_key) VALUES ('gender', false, '',null);
INSERT INTO setting (key, isrevealed, value, parent_key) VALUES('name',false,'', 'gender');

