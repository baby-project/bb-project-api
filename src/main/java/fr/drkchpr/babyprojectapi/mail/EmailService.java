package fr.drkchpr.babyprojectapi.mail;

import fr.drkchpr.babyprojectapi.resource.gender.model.Gender;
import fr.drkchpr.babyprojectapi.resource.gender.repository.IGenderRepository;
import fr.drkchpr.babyprojectapi.resource.settings.model.Setting;
import fr.drkchpr.babyprojectapi.utils.StringUtils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Service
@Slf4j
public class EmailService {


    private final IGenderRepository genderRepository;
    private final EmailHtmlSender emailHtmlSender;

    @Autowired
    public EmailService(final IGenderRepository genderRepository, final EmailHtmlSender emailHtmlSender){
        this.genderRepository = genderRepository;
        this.emailHtmlSender = emailHtmlSender;
    }


    /**
     * Gestion de l'envoie des mails en fonction de la key du setting
     * @param setting le paramètre de type "key"
     * @throws IOException si la ressource /static/mail.properties n'est pas trouvée
     */
    public void sendMail(final Setting setting) throws IOException {
        switch (setting.getKey()) {
            case "gender":
                sendGenderMail(setting);
                break;
            case "name":
            default:
                log.warn("Tentative d'envoi de mails pour le setting " + setting.getKey());
        }
    }

    /**
     * Gestion de l'envoie des mails pour les votes "gender"
     *
     * @param setting setting key "gender"
     * @throws IOException si la ressource /static/mail.properties n'est pas trouvée
     */
    void sendGenderMail(final Setting setting) throws IOException {
        // générer les données communes
        Resource resource = new ClassPathResource("/static/mail.properties");
        final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
        final String resultatGagnant = "G".equals(setting.getValue()) ? properties.getProperty("gender.G") : properties.getProperty("gender.B");
        final String resultatPerdant = "G".equals(setting.getValue()) ? properties.getProperty("gender.B") : properties.getProperty("gender.G");
        final String mailTitle = properties.getProperty("mail.gender.title") + " " + resultatGagnant + "!!";

        // traiter les gagnants
        final List<Gender> gagnants =  this.genderRepository.findByChoice(setting.getValue());
        for(Gender gagnant: gagnants){
            Context context = new Context();
            context.setVariable("title", mailTitle);
            context.setVariable("prenom", gagnant.getFname());
            context.setVariable("choix", resultatGagnant);
            context.setVariable("urlUser", generateBase64URL(gagnant));
            emailHtmlSender.send(gagnant.getEmail(), mailTitle, "email/genderSuccess", context);
        }
        // traiter les perdants
        final List<Gender> perdants =  this.genderRepository.findByChoice("G".equals(setting.getValue()) ? "B": "G");
        for(Gender perdant: perdants){
            Context context = new Context();
            context.setVariable("title", mailTitle);
            context.setVariable("prenom", perdant.getFname());
            context.setVariable("choix", resultatPerdant);
            context.setVariable("resultat", resultatGagnant);
            context.setVariable("urlUser", generateBase64URL(perdant));
            emailHtmlSender.send(perdant.getEmail(), mailTitle, "email/genderEchec", context);
        }

    }

    /**
     *
     * @param gender le gender conteant les nom, prénom et email
     * @return une chaine en base 64 conteannt les infos longueur, nom, prenom et email
     */
    String generateBase64URL(final Gender gender) {
        int longueur = gender.getName().length() + gender.getFname().length() + gender.getEmail().length();
        longueur += 3; // on compte les ";"
        return StringUtils.toBase64(longueur +";"+gender.getName()+";"+gender.getFname()+";"+gender.getEmail());
    }

}
