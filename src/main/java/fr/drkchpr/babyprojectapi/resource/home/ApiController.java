package fr.drkchpr.babyprojectapi.resource.home;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@CrossOrigin(origins = "https://bb.lhoir.me", maxAge = 3600)
@RestController()
public class ApiController {

    @GetMapping(name = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> apiHome(){
        return HomeModel.getInstance().getApiInfos();
    }
}
