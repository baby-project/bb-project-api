package fr.drkchpr.babyprojectapi.resource.home;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Slf4j
public class HomeModel {
    private List<String> infos;
    private static HomeModel instance;

    private HomeModel(){
        final List<String> liste = new ArrayList<>();
        try {
            Resource resource = new ClassPathResource("/api.properties");
            final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
            liste.add(properties.getProperty("api.name"));
            liste.add(properties.getProperty("api.version"));
            liste.add(properties.getProperty("api.buildtime"));
        } catch (IOException | NullPointerException e ) {
            log.error(e.getMessage(),e);
            liste.add("informations not found");
        }
        this.infos = liste;
    }

    public static HomeModel getInstance(){
        if(instance == null){
            instance = new HomeModel();
        }
        return instance;
    }

    public List<String> getApiInfos(){
        return infos;
    }
}
