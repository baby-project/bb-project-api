package fr.drkchpr.babyprojectapi.resource.gender.service;

import fr.drkchpr.babyprojectapi.resource.gender.exceptions.GenderClosedException;
import fr.drkchpr.babyprojectapi.resource.gender.exceptions.GenderDejaVoteException;
import fr.drkchpr.babyprojectapi.resource.gender.exceptions.GenderIntrouvableException;
import fr.drkchpr.babyprojectapi.resource.gender.exceptions.GenderVideException;
import fr.drkchpr.babyprojectapi.resource.gender.model.Gender;
import fr.drkchpr.babyprojectapi.resource.gender.repository.IGenderRepository;
import fr.drkchpr.babyprojectapi.resource.settings.service.SettingService;
import fr.drkchpr.babyprojectapi.utils.ObfuscatorUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Slf4j
@Service
public class GenderService {

    public static final String CET_EMAIL_A_DEJA_ETE_UTILISE_POUR_VOTER = "Cet email a déjà été utilisé pour voter";
    public static final String IL_N_Y_A_AUCUNE_DONNE_GENDER_EN_BASE = "Il n'y a aucune donnée Gender en base.";
    public static final String LE_VOTE_GENDER_D_ID = "Le vote Gender d'id '";
    public static final String N_EXISTE_PAS = "' n'existe pas.";
    public static final String SLASH_ID = "/{id}";
    public static final String ADMIN = "ADMIN";

    private final IGenderRepository genderRepository;
    private final SettingService settingService;

    @Autowired
    public GenderService(final IGenderRepository genderRepository, final SettingService settingService){
        this.genderRepository = genderRepository;
        this.settingService = settingService;
    }

    /**
     * Liste les gender en base, retourne une 204 si il n'y a pas de contenu
     * @return Liste de Gender
     */
    public List<Gender> listerGender(final SecurityContextHolderAwareRequestWrapper request){
        final List<Gender> liste = genderRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));

        if(liste.isEmpty())  throw new GenderVideException(IL_N_Y_A_AUCUNE_DONNE_GENDER_EN_BASE);

        log.info(request.toString());
        log.info(String.valueOf(request.isUserInRole(ADMIN)));
        if(!request.isUserInRole(ADMIN)) {
            for (Gender gender : liste) {
                this.obfuscGender(gender);
            }
        }
        return liste;
    }

    /**
     *
     * @param id l'id du vote à retrouver
     * @param request SecurityContextHolderAwareRequestWrapper
     * @return Gender le vote numéro {id}
     */
    public Gender afficherUnGender(final int id, final SecurityContextHolderAwareRequestWrapper request){
        final Gender gender =  this.genderRepository.findById(id);

        if(null == gender) throw new GenderIntrouvableException(LE_VOTE_GENDER_D_ID + id + N_EXISTE_PAS);

        if(!request.isUserInRole(ADMIN)){
            this.obfuscGender(gender);
        }

        return gender;

    }

    /**
     * @param gender le vote gender à créer
     * @return ResponseEntity
     */
    public ResponseEntity<Void> creerGender(final Gender gender){

        if(this.settingService.isGenderRevealed()){
            throw new GenderClosedException("Le vote n'est plus permis");
        }

        if(null != genderRepository.findByEmail(gender.getEmail())){
            throw new GenderDejaVoteException(CET_EMAIL_A_DEJA_ETE_UTILISE_POUR_VOTER);
        }

        final Gender genderResult = genderRepository.save(gender);

        if(genderResult.getId() == 0) return ResponseEntity.noContent().build();

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path(SLASH_ID)
                .buildAndExpand(genderResult.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    /**
     * Inverse le statut du vote Gender si l'ID existe
     * @param id l'id du gender
     */
    public void patchGender(final int id){
        final Gender gender =  genderRepository.findById(id);
        if(null == gender) throw new GenderIntrouvableException(LE_VOTE_GENDER_D_ID + id + N_EXISTE_PAS);

        genderRepository.patchGender(!gender.isStatus(), gender.getId());
    }

    /**
     * Supprime le vote Gender si l'ID existe
     * @param id l'id du gender
     */
    public void deleteGender(final int id){
        if(this.settingService.isGenderRevealed()){
            throw new GenderClosedException("La suppression d'un vote n'est plus permise.");
        }
        if(genderRepository.existsById(id)) genderRepository.deleteById(id);
        else { throw new GenderIntrouvableException(LE_VOTE_GENDER_D_ID + id + N_EXISTE_PAS); }
    }


    /**
     * Méthode qui permet de cacher les données sensibles de l'objet gender
     *
     * @param gender l'objet Gender à obfusquer
     */
    public void obfuscGender(final Gender gender){
        gender.setEmail(ObfuscatorUtils.obfuscatingEmail(gender.getEmail()));
        gender.setName(ObfuscatorUtils.obfuscatingString(gender.getName()));
    }
}
