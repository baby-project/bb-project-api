package fr.drkchpr.babyprojectapi.resource.gender.controller;

import fr.drkchpr.babyprojectapi.resource.gender.model.Gender;
import fr.drkchpr.babyprojectapi.resource.gender.service.GenderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api()
@CrossOrigin(origins = "https://bb.lhoir.me", maxAge = 3600)
@RestController
@PreAuthorize("isAuthenticated()")
@RequestMapping("/gender")
public class GenderController {



    public static final String LE_VOTE_GENDER_D_ID = "Le vote Gender d'id '";
    public static final String N_EXISTE_PAS = "' n'existe pas.";
    public static final String SLASH_ID = "/{id}";

    private final GenderService genderService;

    @Autowired
    public GenderController(final GenderService genderService){
        this.genderService = genderService;
    }


    @GetMapping(value = "")
    public List<Gender> listerGender(final SecurityContextHolderAwareRequestWrapper request){
        return genderService.listerGender(request);
    }

    @GetMapping(value = SLASH_ID)
    public Gender afficherUnGender(@PathVariable final int id, final SecurityContextHolderAwareRequestWrapper request){
        return genderService.afficherUnGender(id, request);
    }

    @PreAuthorize("permitAll()")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> creerGender(@Valid @RequestBody Gender gender){
        return this.genderService.creerGender(gender);
    }

    @PatchMapping(value = SLASH_ID)
    public void patchGender(@PathVariable int id){
        this.genderService.patchGender(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = SLASH_ID)
    public void deleteGender(@PathVariable int id){
       this.genderService.deleteGender(id);
    }
}
