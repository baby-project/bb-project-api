package fr.drkchpr.babyprojectapi.resource.gender.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class GenderIntrouvableException extends RuntimeException {
    public GenderIntrouvableException(final String s) {
        super(s);
    }
}
