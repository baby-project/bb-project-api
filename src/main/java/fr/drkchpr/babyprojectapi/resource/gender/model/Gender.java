package fr.drkchpr.babyprojectapi.resource.gender.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@ToString(of = {"id","email","name","fname","choice","registerDate","status"})
@EqualsAndHashCode(of = {"id","email","name","fname","choice","registerDate","status"})
public class Gender {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Email
    @Getter @Setter
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

    @Length(min=3, max=30, message="Le nom doit être d'une longueur comprise entre {min} et {max} caractères")
    @Getter @Setter
    @Column(name = "NAME", nullable = false)
    private String name;

    @Length(min=3, max=30)
    @Getter @Setter
    @Column(name = "FNAME", nullable = false)
    private String fname;

    @Length(min=1, max=1)
    @Getter @Setter
    @Column(name = "CHOICE", nullable = false)
    private String choice;

    @Getter @Setter
    @Column(name = "REGISTER_DATE", nullable = false, columnDefinition = "timestamp")
    private LocalDateTime registerDate;

    @Getter @Setter
    @Column(name = "STATUS", nullable = false)
    private boolean status;

    public Gender() {
    }

    public Gender(Gender gender){
        this.id = gender.getId();
        this.email = gender.getEmail();
        this.name = gender.getName();
        this.fname = gender.getFname();
        this.choice = gender.getChoice();
        this.registerDate = gender.getRegisterDate();
        this.status = gender.isStatus();
    }

}
