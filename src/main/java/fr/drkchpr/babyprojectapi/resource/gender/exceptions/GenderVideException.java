package fr.drkchpr.babyprojectapi.resource.gender.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class GenderVideException extends RuntimeException {
    public GenderVideException(String s) {
        super(s);
    }
}
