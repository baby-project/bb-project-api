package fr.drkchpr.babyprojectapi.resource.gender.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class GenderClosedException extends RuntimeException {
    public GenderClosedException(String s) {
        super(s);
    }
}
