package fr.drkchpr.babyprojectapi.resource.gender.repository;

import fr.drkchpr.babyprojectapi.resource.gender.model.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface IGenderRepository extends JpaRepository<Gender, Integer> {

    Gender findById(final int id);

    Gender findByEmail(final String email);

    @Query("from Gender g where g.choice= :choice and g.status = true")
    List<Gender> findByChoice(@Param("choice") final String choice);

    @Transactional
    @Modifying
    @Query("UPDATE Gender g set g.status = :status WHERE g.id = :id")
    void patchGender(@Param("status") final boolean status, @Param("id") final int id);
}
