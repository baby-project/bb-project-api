package fr.drkchpr.babyprojectapi.resource.names.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NamesDejaVoteException extends RuntimeException {
    public NamesDejaVoteException(String s) {
        super(s);
    }
}
