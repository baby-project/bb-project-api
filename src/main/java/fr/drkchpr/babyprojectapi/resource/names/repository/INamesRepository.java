package fr.drkchpr.babyprojectapi.resource.names.repository;

import fr.drkchpr.babyprojectapi.resource.names.model.Names;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface INamesRepository extends JpaRepository<Names, Integer> {

    Names findById(final int id);

    Names findByEmail(final String email);

    @Transactional
    @Modifying
    @Query("UPDATE Names n set n.status = :status WHERE n.id = :id")
    void patchNames(@Param("status") final boolean status, @Param("id") final int id);

    @Query("FROM Names n JOIN n.choix c WHERE c.choice = :firstname AND n.status = true")
    List<Names> findWinners(@Param("firstname") final String firstname);


    @Transactional
    @Modifying
    @Query("Update Names n set n.findedName = false")
    void resetAllFindedStatus();

    @Query("SELECT COUNT(n) from Names n where n.status is true")
    Integer getNumberOfVotant();

    @Query("SELECT COUNT(n) from Names n where n.findedName is true and n.status is true" )
    Integer getNumberOfWinner();
}
