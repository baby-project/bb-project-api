package fr.drkchpr.babyprojectapi.resource.names.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class NameClosedException extends RuntimeException {
    public NameClosedException(String s) {
        super(s);
    }
}
