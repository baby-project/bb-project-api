package fr.drkchpr.babyprojectapi.resource.names.repository;

import fr.drkchpr.babyprojectapi.resource.names.model.Choice;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

public interface IChoiceRepository extends JpaRepository<Choice, Integer> {

    @Query("SELECT c.choice as choice, COUNT(c) as recurrence from Choice c inner join c.names n where n.status is  true  GROUP BY c.choice HAVING COUNT(c) > 1 ORDER BY COUNT(c) desc, c.choice")
    List<Map<String, Integer>> getTendances();

    @Query("SELECT count( distinct c.choice) from Choice c inner join c.names n where n.status is  true")
    public Integer getNumberOfFnames();



}
