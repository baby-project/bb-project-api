package fr.drkchpr.babyprojectapi.resource.names.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@ToString(of = {"id","email","name","fname","findedName","registerDate","status"})
@EqualsAndHashCode(of = {"id","email","name","fname","findedName","registerDate","status"})
@SequenceGenerator(name="names_id_generator", allocationSize=1)
public class Names {

    @Id
    @Getter @Setter
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "names_id_generator")
    private Integer id;

    @Email
    @Getter @Setter
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

    @Length(min=3, max=30, message="Le nom doit être d'une longueur comprise entre {min} et {max} caractères")
    @Getter @Setter
    @Column(name = "NAME", nullable = false)
    private String name;

    @Length(min=3, max=30, message="Le prénom doit être d'une longueur comprise entre {min} et {max} caractères")
    @Getter @Setter
    @Column(name = "FNAME", nullable = false)
    private String fname;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.MERGE, CascadeType.ALL},fetch = FetchType.LAZY)
    @JoinColumn()
    @Getter @Setter
    private Set<Choice> choix;

    @Column(name = "FINDED_NAME")
    @Getter @Setter
    private boolean findedName;

    @Column(name = "REGISTER_DATE", nullable = false, columnDefinition = "timestamp")
    @Getter @Setter
    private LocalDateTime registerDate;

    @Column(name = "STATUS", nullable = false)
    @Getter @Setter
    private boolean status;

    public Names() {
    }

    public Names(final Integer id, final String email, final String name, final String fname, final List<String> choix, final boolean findedName, final LocalDateTime registerDate, final boolean status ){
        this.id = id;
        this.email = email;
        this.name = name;
        this.fname = fname;
        this.findedName = findedName;
        this.registerDate = registerDate;
        this.status = status;
        this.choix = new HashSet<>();
        for (String str: choix) {
            this.choix.add(new Choice(str));
        }
        this.choix.forEach(x -> x.setNames(this));

    }

    public Names(final Names names){
        this.id = names.getId();
        this.email = names.getEmail();
        this.name = names.getName();
        this.fname = names.getFname();
        this.choix = names.getChoix();
        this.findedName = names.isFindedName();
        this.registerDate = names.getRegisterDate();
        this.status = names.isStatus();
    }

    public void populateChild(){
        this.choix.forEach(x -> x.setNames(this));
    }
}
