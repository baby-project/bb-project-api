package fr.drkchpr.babyprojectapi.resource.names.model;

import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@ToString(of={"numberOfVotes","numberOfFname","numberOfWinner","tendances"})
@EqualsAndHashCode(of={"numberOfVotes","numberOfFname","numberOfWinner","tendances"})
public class Stats {
    @Getter @Setter
    private Integer numberOfVotes;

    @Getter @Setter
    private Integer numberOfFname;

    @Getter @Setter
    private Integer numberOfWinner;

    @Getter @Setter
    private List<Map<String,Integer>> tendances;

}
