package fr.drkchpr.babyprojectapi.resource.names.controller;


import fr.drkchpr.babyprojectapi.resource.names.model.Names;
import fr.drkchpr.babyprojectapi.resource.names.model.Stats;

import fr.drkchpr.babyprojectapi.resource.names.service.NamesService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Api()
@CrossOrigin(origins = "https://bb.lhoir.me", maxAge = 3600)
@RestController
@PreAuthorize("isAuthenticated()")
@RequestMapping("/names")
public class NamesController {

    public static final String SLASH_ID = "/{id}";

    private final NamesService namesService;

    @Autowired
    public NamesController(final NamesService namesService){
        this.namesService = namesService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Names> listerNames(final SecurityContextHolderAwareRequestWrapper request){
        return this.namesService.listerNames(request);
    }
    @GetMapping(value = "/stats", produces = MediaType.APPLICATION_JSON_VALUE)
    public Stats getStats(final SecurityContextHolderAwareRequestWrapper request){
        return this.namesService.getStats(request);
    }

    @PreAuthorize("permitAll()")
    @PostMapping(value = "",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> creerNames(@Valid @RequestBody Names names){
        return this.namesService.creerNames(names);
    }

    @PatchMapping(value = SLASH_ID)
    public void patchNames(@PathVariable int id){
        this.namesService.patchNames(id);
    }


    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(value = SLASH_ID)
    public void deleteNames(@PathVariable int id){
        this.namesService.deleteNames(id);
    }

}

