package fr.drkchpr.babyprojectapi.resource.names.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@JsonPropertyOrder({ "choice" })
@ToString(of = {"choiceId","choice"})
@EqualsAndHashCode(of =  {"choiceId","choice"})
@SequenceGenerator(name="choice_id_generator", allocationSize=1)
public class Choice {

    @Id
    @Getter @Setter
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "choice_id_generator")
    private long choiceId;

    @Getter @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private Names names;


    @Getter @Setter
    private String choice;

    public Choice(){}

    public Choice(final String choix){
        this.choice = choix;
    }

}
