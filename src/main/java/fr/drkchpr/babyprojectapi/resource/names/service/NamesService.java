package fr.drkchpr.babyprojectapi.resource.names.service;

import fr.drkchpr.babyprojectapi.resource.names.exceptions.NameClosedException;
import fr.drkchpr.babyprojectapi.resource.names.exceptions.NamesDejaVoteException;
import fr.drkchpr.babyprojectapi.resource.names.exceptions.NamesIntrouvableException;
import fr.drkchpr.babyprojectapi.resource.names.exceptions.NamesVideException;
import fr.drkchpr.babyprojectapi.resource.names.model.Choice;
import fr.drkchpr.babyprojectapi.resource.names.model.Names;
import fr.drkchpr.babyprojectapi.resource.names.model.Stats;
import fr.drkchpr.babyprojectapi.resource.names.repository.IChoiceRepository;
import fr.drkchpr.babyprojectapi.resource.names.repository.INamesRepository;
import fr.drkchpr.babyprojectapi.resource.settings.service.SettingService;
import fr.drkchpr.babyprojectapi.utils.ObfuscatorUtils;
import fr.drkchpr.babyprojectapi.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@Slf4j
@Service
public class NamesService {

    public static final String CET_EMAIL_A_DEJA_ETE_UTILISE_POUR_VOTER = "Cet email a déjà été utilisé pour voter";
    public static final String IL_N_Y_A_AUCUNE_DONNE_NAMES_EN_BASE = "Il n'y a aucune donnée Names en base.";
    public static final String LE_VOTE_NAMES_D_ID = "Le vote Names d'id '";
    public static final String N_EXISTE_PAS = "' n'existe pas.";
    public static final String SLASH_ID = "/{id}";
    public static final String ADMIN = "ADMIN";

    private final INamesRepository namesRepository;
    private final IChoiceRepository choiceRepository;
    private final SettingService settingService;

    @Autowired
    public NamesService(final INamesRepository namesRepository, final SettingService settingService, final IChoiceRepository choiceRepository) {
        this.namesRepository = namesRepository;
        this.settingService = settingService;
        this.choiceRepository = choiceRepository;
    }

    /**
     * Liste tous les votes names
     * @param request gestion des droits
     * @return liste de vote de prénom
     */
    public List<Names> listerNames(final SecurityContextHolderAwareRequestWrapper request){
        final List<Names> liste = namesRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));

        if(liste.isEmpty()){
            throw new NamesVideException(IL_N_Y_A_AUCUNE_DONNE_NAMES_EN_BASE);
        }

        if(!request.isUserInRole(ADMIN)) {
            for (Names names : liste) {
                this.obfusNames(names);
            }
        }

        return liste;
    }

    public Stats getStats(final SecurityContextHolderAwareRequestWrapper request){
        final Stats stats = new Stats();
        stats.setNumberOfFname(this.choiceRepository.getNumberOfFnames());
        stats.setNumberOfVotes(this.namesRepository.getNumberOfVotant());
        stats.setNumberOfWinner(this.namesRepository.getNumberOfWinner());

        if(request.isUserInRole(ADMIN)){
            stats.setTendances(this.choiceRepository.getTendances());
        }
        return stats;
    }


    /**
     *
     * @param names le vote de prénom à créer
     * @return le résultat de l'enregistrement
     */
    public ResponseEntity<Void> creerNames(final Names names){
        if(this.settingService.isNameRevealed()){
            throw new NameClosedException("Le vote du prénom n'est plus possible");
        }

        if(null != namesRepository.findByEmail(names.getEmail())){
            throw new NamesDejaVoteException(CET_EMAIL_A_DEJA_ETE_UTILISE_POUR_VOTER);
        }

        simplifyChoices(names);

        this.settingService.checkNameIsFinded(names);

        names.populateChild();

        final Names namesResult = namesRepository.save(names);

        final URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path(SLASH_ID)
                .buildAndExpand(namesResult.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    /**
     * Lowercae, retrait des accents et tri sur la liste choices de l'objet Name
     * @param names l'objet Names dont on doit modifier la liste choices
     */
    void simplifyChoices(final Names names) {

        for(Choice choice : names.getChoix()){
            choice.setChoice(StringUtils.unaccent(choice.getChoice().toLowerCase()).trim());
        }
    }


    /**
     * Inverse le statut du vote Names si l'ID existe
     * @param id l'id du gender
     */
    public void patchNames(final int id){
        final Names names =  namesRepository.findById(id);
        if(null == names) throw new NamesIntrouvableException(LE_VOTE_NAMES_D_ID + id + N_EXISTE_PAS);

        namesRepository.patchNames(!names.isStatus(), names.getId());
    }

    /**
     * Supprime le vote Names si l'ID existe
     * @param id l'id du names
     */
    public void deleteNames(final int id){
        if(this.settingService.isNameRevealed()){
            throw new NameClosedException("La suppression d'un vote n'est plus permise.");
        }
        if(namesRepository.existsById(id)) namesRepository.deleteById(id);
        else { throw new NamesIntrouvableException(LE_VOTE_NAMES_D_ID + id + N_EXISTE_PAS); }
    }

    /**
     * Méthode qui permet de cacher les données sensibles de l'objet Names
     * @param names l'objet Names à obfusquer
     */
    void obfusNames(final Names names){

        names.setEmail(ObfuscatorUtils.obfuscatingEmail(names.getEmail()));
        names.setName(ObfuscatorUtils.obfuscatingString(names.getName()));

        for(Choice choice : names.getChoix()){
            choice.setChoice(ObfuscatorUtils.obfuscatingString(choice.getChoice()));
        }
    }
}
