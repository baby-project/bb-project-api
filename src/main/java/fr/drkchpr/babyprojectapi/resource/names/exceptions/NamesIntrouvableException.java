package fr.drkchpr.babyprojectapi.resource.names.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class NamesIntrouvableException extends RuntimeException {
    public NamesIntrouvableException(final String s) {
        super(s);
    }
}
