package fr.drkchpr.babyprojectapi.resource.parent.controller;

import fr.drkchpr.babyprojectapi.resource.parent.exception.LoginException;
import fr.drkchpr.babyprojectapi.resource.parent.model.Parent;
import fr.drkchpr.babyprojectapi.resource.parent.repository.IParentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@CrossOrigin(origins = "https://bb.lhoir.me", maxAge = 3600)
@RestController()
@RequestMapping("/login")
public class ParentController {

    @Autowired
    private IParentRepository parentRepository;

    @PostMapping(name = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Parent login(@Valid @RequestBody Parent parent){
        final Parent parentOut = parentRepository.findByUsername(parent.getUsername());

        if(null == parentOut || ! new BCryptPasswordEncoder().matches(parent.getPassword(),parentOut.getPassword())) {
            throw new LoginException("Utilisateur ou mot de passe invalide");
        }
        parent.setRole(parentOut.getRole());
        return parent;
    }
}
