package fr.drkchpr.babyprojectapi.resource.parent.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * Représente un parent en base de données
 * = une personne qui a acces à l'administration de l'application
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "parents")
@Table(name = "parents")
public class Parent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotEmpty
    @Column(name = "username", nullable = false, unique = true, length = 30)
    private String username;

    @NotNull
    @NotEmpty
    @Column(name = "password", nullable = false, unique = false, length = 60)
    private String password;

    @Column(name="role",unique = false)
    private String role;


}
