package fr.drkchpr.babyprojectapi.resource.parent.repository;

import fr.drkchpr.babyprojectapi.resource.parent.model.Parent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IParentRepository extends JpaRepository<Parent, Long> {

    Parent findByUsername(final String username);
}
