package fr.drkchpr.babyprojectapi.resource.settings.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class SettingAlreadyRevealedException extends RuntimeException {
    public SettingAlreadyRevealedException(String s) {
        super(s);
    }

}
