package fr.drkchpr.babyprojectapi.resource.settings.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SettingNotReadyException extends RuntimeException {
    public SettingNotReadyException(String s) {
        super(s);
    }
}
