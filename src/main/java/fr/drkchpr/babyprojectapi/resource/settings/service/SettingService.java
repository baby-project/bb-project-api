package fr.drkchpr.babyprojectapi.resource.settings.service;

import fr.drkchpr.babyprojectapi.mail.EmailService;
import fr.drkchpr.babyprojectapi.resource.names.model.Names;
import fr.drkchpr.babyprojectapi.resource.names.repository.INamesRepository;
import fr.drkchpr.babyprojectapi.resource.settings.exceptions.SettingAlreadyRevealedException;
import fr.drkchpr.babyprojectapi.resource.settings.exceptions.SettingIntrouvableException;
import fr.drkchpr.babyprojectapi.resource.settings.exceptions.SettingNotReadyException;
import fr.drkchpr.babyprojectapi.resource.settings.model.Setting;
import fr.drkchpr.babyprojectapi.resource.settings.repository.ISettingRepository;
import fr.drkchpr.babyprojectapi.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class SettingService {

    public static final String LE_SETTING = "Le setting '";
    private final ISettingRepository settingRepository;
    private final INamesRepository namesRepository;
    private final EmailService emailService;

    @Autowired
    public SettingService(final ISettingRepository settingRepository, final INamesRepository namesRepository, final EmailService emailService) {
        this.settingRepository = settingRepository;
        this.namesRepository = namesRepository;
        this.emailService = emailService;
    }

    /**
     * Renvoie la liste des setting de l'application
     *
     * @param request SecurityContextHolderAwareRequestWrapper
     * @return Liste de settings
     */
    public List<Setting> listerSetting(final SecurityContextHolderAwareRequestWrapper request) {
        List<Setting> retour = settingRepository.findAll(Sort.by(Sort.Direction.DESC, "key"));

        if (!request.isUserInRole("ADMIN")) {
            obfuscatingNotRevealedData(retour);
        }

        return retour;
    }

    /**
     * Met à jour un setting
     *
     * @param setting Le setting à modifier
     * @return setting Retourne le setting modifié
     */
    public Setting patchSetting(final Setting setting) {

        final Setting dbSetting = this.settingRepository.findByKey(setting.getKey());

        checkSettingExists(setting, dbSetting);

        checkSettingAlreadyRevealed(dbSetting);

        // revealing
        if (setting.isRevealed()) {

            checkSettingValue(setting, dbSetting);

            checkSettingParent(dbSetting);

            if (StringUtils.isNotBlank(setting.getValue())) {
                dbSetting.setValue(setting.getValue());
            }

            dbSetting.setRevealed(setting.isRevealed());

            this.settingRepository.save(dbSetting);

            updateNamesWinner(setting, dbSetting);

            try {
                this.emailService.sendMail(dbSetting);
            } catch (IOException ioe){
                log.error(ioe.getMessage(), ioe);
            }

        } else {
            // MaJ only
            dbSetting.setValue(setting.getValue());
            this.settingRepository.save(dbSetting);
            updateNamesWinner(setting, dbSetting);
        }

        return dbSetting;
    }

    /**
     * Si le setting possède la key "name", on reset les gagants en base et on vient calculer les gagnants en fonction du prénom
     *
     * @param setting   le paramètre d'entrée
     * @param dbSetting le paramètre en base
     */
    void updateNamesWinner(final Setting setting, final Setting dbSetting) {
        if ("name".equals(setting.getKey()) && StringUtils.isNotBlank(dbSetting.getValue())) {
            this.namesRepository.resetAllFindedStatus();
            final List<Names> winners = this.namesRepository.findWinners(StringUtils.unaccent(dbSetting.getValue().toLowerCase()));
            for (Names name : winners) {
                name.setFindedName(true);
            }
            this.namesRepository.saveAll(winners);
        }
    }

    /**
     * @return true si le genre du bébé a été révélé, false sinon
     */
    public boolean isGenderRevealed() {
        return isRevealed("gender");
    }

    /**
     * @return true si le prénom du bébé a été révélé, false sinon
     */
    public boolean isNameRevealed() {
        return isRevealed("name");
    }

    /**
     * @param key la clef du setting à vérifier
     * @return boolean
     */
    boolean isRevealed(final String key) {
        final Setting setting = this.settingRepository.findByKey(key);
        return null != setting && setting.isRevealed();
    }

    /**
     * @param setting   Setting to be changed
     * @param dbSetting Setting in database
     * @throws SettingIntrouvableException est levée si le setting n'existe pas en base
     */
    void checkSettingExists(Setting setting, Setting dbSetting) {
        if (null == dbSetting) {
            log.warn(LE_SETTING + setting.getKey() + "' n'existe pas.");
            throw new SettingIntrouvableException(LE_SETTING + setting.getKey() + "' n'existe pas.");
        }
    }

    /**
     * @param dbSetting Setting in database
     * @throws SettingAlreadyRevealedException est levée si le setting est déja révélé
     */
    void checkSettingAlreadyRevealed(Setting dbSetting) {
        if (dbSetting.isRevealed()) {
            log.warn(LE_SETTING + dbSetting.getKey() + "' ne peut être modifié car il est déjà révélé.");
            throw new SettingAlreadyRevealedException(LE_SETTING + dbSetting.getKey() + "' ne peut être modifié car il est déjà révélé.");
        }
    }

    /**
     * @param setting   Setting to be changed
     * @param dbSetting Setting in database
     * @throws SettingNotReadyException est levée si il y a un soucis sur la valeur du setting
     */
    void checkSettingValue(final Setting setting, final Setting dbSetting) {
        if (StringUtils.isBlank(setting.getValue()) && StringUtils.isBlank(dbSetting.getValue())) {
            log.warn(LE_SETTING + dbSetting.getKey() + "' ne peut être révélé car sa valeur est null");
            throw new SettingNotReadyException(LE_SETTING + dbSetting.getKey() + "' ne peut être révélé car sa valeur est null.");
        }
    }

    /**
     * @param dbSetting Setting in database
     * @throws SettingNotReadyException est levée si il y a un soucis sur le parent du setting
     */
    void checkSettingParent( final Setting dbSetting) {
        if (null != dbSetting.getParent() && !dbSetting.getParent().isRevealed()) {
            log.warn(LE_SETTING + dbSetting.getKey() + "' ne peut être révélé car son parent ('" + dbSetting.getParent().getKey() + "') ne l'est pas.");
            throw new SettingNotReadyException(LE_SETTING + dbSetting.getKey() + "' ne peut être révélé car son parent ('" + dbSetting.getParent().getKey() + "') ne l'est pas.");
        }
    }

    /**
     * Vient mettre à jour le vote en fontion des prénoms choisis
     *
     * @param name le vote à vérifier
     */
    public void checkNameIsFinded(final Names name) {
        this.checkNameIsFinded(name, this.settingRepository.findByKey("name"));
    }

    /**
     * @param name        le vote avec les prénoms à vérifier
     * @param settingName le setting avec les nom choisis par les parents
     */
    void checkNameIsFinded(final Names name, final Setting settingName) {
        final List<String> firstnameInBase = Arrays.asList(settingName.getValue().split(";"));
        if (name.getChoix().contains(firstnameInBase.get(0))) {
            name.setFindedName(true);
        }
    }

    /**
     * @param settings liste des settings à anonymiser
     */
    void obfuscatingNotRevealedData(final List<Setting> settings) {
        for (Setting setting : settings) {
            if (!setting.isRevealed()) {
                setting.setValue("");
            }
        }
    }


}
