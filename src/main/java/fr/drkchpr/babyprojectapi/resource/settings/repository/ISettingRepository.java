package fr.drkchpr.babyprojectapi.resource.settings.repository;

import fr.drkchpr.babyprojectapi.resource.settings.model.Setting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISettingRepository extends JpaRepository<Setting, String> {

    Setting findByKey(final String key);

    boolean existsByKey(final String Key);

    boolean existsByKeyAndIsRevealed(final String key, final boolean bool);
}
