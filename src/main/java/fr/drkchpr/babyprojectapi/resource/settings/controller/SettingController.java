package fr.drkchpr.babyprojectapi.resource.settings.controller;

import fr.drkchpr.babyprojectapi.resource.settings.model.Setting;
import fr.drkchpr.babyprojectapi.resource.settings.service.SettingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api()
@CrossOrigin(origins = "https://bb.lhoir.me", maxAge = 3600)
@RestController
@RequestMapping("/settings")
public class SettingController {

    public static final String SLASH_ID = "/{id}";

    private final SettingService settingService;

    @Autowired
    public SettingController(final SettingService settingService) {
        this.settingService = settingService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Setting> listerSettings(final SecurityContextHolderAwareRequestWrapper request) {
        return this.settingService.listerSetting(request);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void patchSetting(@Valid @RequestBody Setting setting) {
        this.settingService.patchSetting(setting);
    }
}
