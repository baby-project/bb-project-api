package fr.drkchpr.babyprojectapi.resource.settings.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class SettingIntrouvableException extends RuntimeException {
    public SettingIntrouvableException(final String s) {
        super(s);
    }
}
