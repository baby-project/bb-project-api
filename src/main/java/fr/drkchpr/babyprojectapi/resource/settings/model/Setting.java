package fr.drkchpr.babyprojectapi.resource.settings.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@ToString(of = {"key", "value", "isRevealed", "parent"})
@EqualsAndHashCode(of = {"key", "value","isRevealed","parent"})
public class Setting {

    @Id
    @Column(name = "KEY", nullable = false, unique = true)
    @Length(min = 1, max = 250, message = "La clef doit être d'une longueur comprise entre {min} et {max} caractères")
    @Getter
    @Setter
    private String key;

    @Column(name = "ISREVEALED", nullable = false)
    @Getter
    @Setter
    private boolean isRevealed;

    @Column(name = "VALUE", nullable = false)
    @Length(max = 250, message = "La valeur doit être d'une longueur max de {max} caractères")
    @Getter
    @Setter
    private String value;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(unique = true)
    @Getter @Setter
    private Setting parent;

    public Setting() {
    }

    public Setting(final Setting setting) {
        this.key = setting.getKey();
        this.isRevealed = setting.isRevealed();
        this.value = setting.getValue();
        this.parent = setting.getParent();
    }

}
