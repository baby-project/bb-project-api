package fr.drkchpr.babyprojectapi.utils;

import java.util.regex.Pattern;

public class ObfuscatorUtils {

    public static final String REGEX_EMAIL = "^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    private ObfuscatorUtils() {}

    /**
     * Anonymise la chaine de caractère s'il s'agit d'un email,<br />
     * Renvoie la chaine sans modification sinon.
     *
     * @param plainEmail la chaine à anonymiser
     * @return String la chaine anonymisée ou non
     */
    public static String obfuscatingEmail(final String plainEmail){
        String obfuscatedEmail = plainEmail;
        if(Pattern.matches(REGEX_EMAIL, plainEmail)){
            final String[] emailSplit = plainEmail.split("@");
            obfuscatedEmail = emailSplit[0].substring(0,1)
                    + "***@"
                    + emailSplit[1].substring(0,0)
                    + "***"
                    + emailSplit[1].substring(emailSplit[1].length()-1);
        }
        return obfuscatedEmail;
    }

    /**
     * Anonymise la chaine de caractère en entrée.
     *
     * @param str la chaine à anonymiser
     * @return la chaine anonymisée
     */
    public static String obfuscatingString(final String str){
        return str.substring(0,1) + "***";
    }
}
