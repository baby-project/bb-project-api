package fr.drkchpr.babyprojectapi.utils;

import java.text.Normalizer;
import java.util.Base64;

public class StringUtils {

    private StringUtils(){}

    /**
     * @param s la chaine à tester
     * @return boolean true si null ou blank ou empty
     */
    public static boolean isBlank(final String s){
        return s == null || s.trim().isEmpty();
    }

    /**
     *
     * @param s la chaine à tester
     * @return boolean false si null ou blank ou empty
     */
    public static boolean isNotBlank(final String s){
        return !isBlank(s);
    }

    /**
     * Retire les accents de la chaine en entrée
     * @param src la chbaine a modifier
     * @return la chaine sans les accents
     */
    public static String unaccent(String src) {
        return Normalizer
                .normalize(src, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }

    /**
     * @param src chaine à encoder
     * @return chaine codée si notBlank, chaine source sinon
     */
    public static String toBase64(final String src) {
        return StringUtils.isBlank(src) ? src: Base64.getEncoder().withoutPadding().encodeToString(src.getBytes());
    }
}
