package fr.drkchpr.babyprojectapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.Scanner;

@Slf4j
public class EncryptMain {

    public static void main(final String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        Scanner sc = new Scanner(System.in);
        log.info("Password to encrypt ?");
        String password = sc.nextLine();
        log.info("Encrypted password : " + bCryptPasswordEncoder.encode(password));
    }
}

