package fr.drkchpr.babyprojectapi.resource.gender.service;

import fr.drkchpr.babyprojectapi.resource.gender.exceptions.GenderIntrouvableException;
import fr.drkchpr.babyprojectapi.resource.gender.exceptions.GenderVideException;
import fr.drkchpr.babyprojectapi.resource.gender.model.Gender;
import fr.drkchpr.babyprojectapi.resource.gender.repository.IGenderRepository;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GenderServiceTest {

    public static final String REGEX_MATCH_STAR = "^.*[\\*].*$";

    @Mock
    private IGenderRepository genderRepository;

    @Mock
    private SecurityContextHolderAwareRequestWrapper requestWrapper;

    @InjectMocks
    private GenderService genderService;

    private final Gender uniqueGender = new Gender(0, "alain@dumont.fr", "Dupont", "Alain", "F", LocalDateTime.now(), true);
    private final Gender modifiableGender = new Gender(0, "alain@dumont.fr", "Dupont", "Alain", "F", LocalDateTime.now(), true);
    private List<Gender> genders;

    @BeforeEach
    public void setup(){
        final List<Gender> genders = new ArrayList<>();
        genders.add(new Gender(10,"alain@dumont.fr", "Dupont", "Alain", "F", LocalDateTime.now(), true));
        genders.add(new Gender(20,"beatrice@petit.me", "Petit", "Beatrice", "G", LocalDateTime.now(), true));
        this.genders = genders;
    }

    @Test
    void listerGender_without_data_should_return_exception(){
        when(this.genderRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(null);
        Assertions.assertThrows(GenderVideException.class, () -> {
            this.genderService.listerGender( this.requestWrapper);
        });
    }

    @Test
    void listerGender_give_list_gender_if_admin(){
        when(this.genderRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(this.genders);
        when(this.requestWrapper.isUserInRole(anyString())).thenReturn(true); // simulate ADMIN
        final List<Gender> results = this.genderService.listerGender(this.requestWrapper);
        Assertions.assertTrue(2 == results.size());
        Assertions.assertTrue(results.get(0).getId() < results.get(1).getId());
        Assertions.assertFalse(Pattern.matches(REGEX_MATCH_STAR,results.get(0).getEmail()));
        Assertions.assertFalse(Pattern.matches(REGEX_MATCH_STAR,results.get(0).getName()));
    }

    @Test
    void afficherUnGender_should_throw_exception_if_gender_not_exist() {
        when(this.genderRepository.findById(anyInt())).thenReturn(null);
        Assertions.assertThrows(GenderIntrouvableException.class, () -> {
            this.genderService.afficherUnGender(0, this.requestWrapper);
        });
    }

    @Test
    void afficherUnGender_give_gender_when_admin() {
        when(this.genderRepository.findById(anyInt())).thenReturn(this.modifiableGender);
        when(this.requestWrapper.isUserInRole(anyString())).thenReturn(true); // simulate ADMIN
        Assertions.assertEquals(this.uniqueGender,this.genderService.afficherUnGender(anyInt(),this.requestWrapper));
    }

    @Test
    void afficherUnGender_give_gender_obfuscated_when_not_admin() {
        when(this.genderRepository.findById(anyInt())).thenReturn(this.modifiableGender);
        when(this.requestWrapper.isUserInRole(anyString())).thenReturn(false); // simulate !ADMIN
        Assertions.assertNotEquals(this.uniqueGender,this.genderService.afficherUnGender(anyInt(),this.requestWrapper));
    }

    @Test
    void obfuscGender_should_obfuscate_email_and_name() {
        this.genderService.obfuscGender(this.modifiableGender);
        Assertions.assertTrue(Pattern.matches(REGEX_MATCH_STAR, this.modifiableGender.getEmail()));
        Assertions.assertTrue(Pattern.matches(REGEX_MATCH_STAR, this.modifiableGender.getName()));
    }

    @Test
    void obfuscGender_should_obfuscate_only_email_and_name() {
        final LocalDateTime now = this.modifiableGender.getRegisterDate();
        this.genderService.obfuscGender(this.modifiableGender);
        assertEquals(0, (int) this.modifiableGender.getId());
        Assertions.assertFalse(Pattern.matches(REGEX_MATCH_STAR, this.modifiableGender.getFname()));
        assertEquals("F", this.modifiableGender.getChoice());
        assertEquals(now, this.modifiableGender.getRegisterDate());
        assertTrue(this.modifiableGender.isStatus());
    }

}