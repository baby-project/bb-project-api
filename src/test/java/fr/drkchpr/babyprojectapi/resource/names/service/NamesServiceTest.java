package fr.drkchpr.babyprojectapi.resource.names.service;


import fr.drkchpr.babyprojectapi.resource.names.exceptions.NamesVideException;
import fr.drkchpr.babyprojectapi.resource.names.model.Choice;
import fr.drkchpr.babyprojectapi.resource.names.model.Names;
import fr.drkchpr.babyprojectapi.resource.names.repository.INamesRepository;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class NamesServiceTest {
//
//    @Mock
//    private INamesRepository namesRepository;
//
//    @Mock
//    private SecurityContextHolderAwareRequestWrapper requestWrapper;
//
//    @InjectMocks
//    private NamesService namesService;
//
//    public static final String REGEX_MATCH_STAR = "^.*[\\*].*$";
//
//    private final Names names1 = new Names(10,"alain@dumont.fr", "Dupont", "Alain", Arrays.asList(new Choice(1,10,"Tintin"), new Choice(2,10,"Milou")),false, LocalDateTime.now(), true);
//    private final Names  names2 = new Names(20,"beatrice@petit.me", "Petit", "Beatrice", Arrays.asList(new Choice(3,20,"Asterix"), new Choice(4,20,"Obelix")), false, LocalDateTime.now(), true);
//    private final List<Names> lNames = Arrays.asList(new Names(names1),new Names(names2));
//
//    @Test
//    void listerNames_without_data_should_return_exception(){
//        when(this.namesRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(new ArrayList<Names>());
//        Assertions.assertThrows(NamesVideException.class, () -> this.namesService.listerNames( this.requestWrapper));
//    }
//
//    @Test
//    void listerNames_give_list_names_if_admin(){
//        when(this.namesRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(this.lNames);
//        when(this.requestWrapper.isUserInRole(anyString())).thenReturn(true); // simulate ADMIN
//        final List<Names> results = this.namesService.listerNames(this.requestWrapper);
//        assertEquals(2, results.size());
//        assertEquals(results.get(0), names1);
//        assertEquals(results.get(1), names2);
//    }
//
//    @Test
//    void listerNames_give_obfuscated_list_names_if_not_admin(){
//        when(this.namesRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(this.lNames);
//        when(this.requestWrapper.isUserInRole(anyString())).thenReturn(false); // simulate ADMIN
//        final List<Names> results = this.namesService.listerNames(this.requestWrapper);
//        assertEquals(2, results.size());
//        assertNotEquals(results.get(0), names1);
//        assertNotEquals(results.get(1), names2);
//    }
//
//    @Test
//    void obfusNames_should_obfuscate_personnal_data_only() {
//        final LocalDateTime now = this.names1.getRegisterDate();
//        this.namesService.obfusNames(this.names1);
//        assertEquals(10, (int) this.names1.getId());
//        Assertions.assertTrue(Pattern.matches(REGEX_MATCH_STAR, this.names1.getName()), "Name should be 'D***'");
//        Assertions.assertFalse(Pattern.matches(REGEX_MATCH_STAR, this.names1.getFname()), "FName should be 'Alain'");
//        for(Choice choice: this.names1.getChoices()){
//            Assertions.assertTrue(Pattern.matches(REGEX_MATCH_STAR,choice.getChoice()), "Choice should be obfuscated with ***");
//        }
//        assertEquals(now, this.names1.getRegisterDate());
//        assertTrue(this.names1.isStatus());
//    }
}