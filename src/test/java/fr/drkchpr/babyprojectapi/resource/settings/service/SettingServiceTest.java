package fr.drkchpr.babyprojectapi.resource.settings.service;

import fr.drkchpr.babyprojectapi.resource.settings.exceptions.SettingAlreadyRevealedException;
import fr.drkchpr.babyprojectapi.resource.settings.exceptions.SettingIntrouvableException;
import fr.drkchpr.babyprojectapi.resource.settings.exceptions.SettingNotReadyException;
import fr.drkchpr.babyprojectapi.resource.settings.model.Setting;
import fr.drkchpr.babyprojectapi.resource.settings.repository.ISettingRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SettingServiceTest {

    @Mock
    private ISettingRepository settingRepository;

    @Mock
    private SecurityContextHolderAwareRequestWrapper requestWrapper;

    @InjectMocks
    private SettingService settingService;

    private final Setting parentNotRevealed = new Setting("parentNotRevealed", false, "value", null);
    private final Setting parentRevealed = new Setting("parentRevealed", true, "value", null);
    private final Setting aloneRevealed = new Setting("aloneRevealed", true, "value",null);
    private final Setting aloneNotValue = new Setting("aloneNotValue", false, "",null);
    private final Setting childRevealedParentNot = new Setting("childRevealedParentNot", true, "value",parentNotRevealed);
    private final Setting childRevealedParentToo = new Setting("childRevealedParentNot", true, "value",parentRevealed);

    @Test
    void isRevealed() {
        when(this.settingRepository.findByKey("keyTrue")).thenReturn(new Setting("keyTrue",true,"",null));
        when(this.settingRepository.findByKey("keyFalse")).thenReturn(new Setting("keyFalse",false,"",null));
        assertTrue(this.settingService.isRevealed("keyTrue"));
        assertFalse(this.settingService.isRevealed("keyFalse"));
    }

    @Test
    void checkSettingExists() {
        Assertions.assertThrows(SettingIntrouvableException.class, () -> this.settingService.checkSettingExists(aloneRevealed,null));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingExists(aloneRevealed, aloneRevealed));
    }

    @Test
    void checkSettingAlreadyRevealed() {
        Assertions.assertThrows(SettingAlreadyRevealedException.class, () -> this.settingService.checkSettingAlreadyRevealed(aloneRevealed));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingAlreadyRevealed(parentNotRevealed));
    }

    @Test
    void obfuscatingNotRevealedData() {
        final List<Setting> liste = new ArrayList<>();
        liste.add(parentNotRevealed);
        liste.add(childRevealedParentNot);

        this.settingService.obfuscatingNotRevealedData(liste);
        assertEquals("", liste.get(0).getValue());
        assertNotEquals("", liste.get(1).getValue());
        assertEquals("", liste.get(1).getParent().getValue());
    }

    @Test
    void checkSettingValue() {
        Assertions.assertThrows(SettingNotReadyException.class, () -> this.settingService.checkSettingValue(aloneNotValue,aloneNotValue));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingValue(aloneRevealed,aloneRevealed));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingValue(aloneRevealed,aloneNotValue));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingValue(aloneNotValue,aloneRevealed));
    }

    @Test
    void checkSettingParent() {
        Assertions.assertThrows(SettingNotReadyException.class, () -> this.settingService.checkSettingParent(childRevealedParentNot));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingParent(aloneRevealed));
        Assertions.assertDoesNotThrow(() -> this.settingService.checkSettingParent(childRevealedParentToo));
    }
}