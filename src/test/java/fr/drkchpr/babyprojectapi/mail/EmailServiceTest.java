package fr.drkchpr.babyprojectapi.mail;

import fr.drkchpr.babyprojectapi.resource.gender.model.Gender;
import fr.drkchpr.babyprojectapi.resource.gender.repository.IGenderRepository;
import fr.drkchpr.babyprojectapi.resource.gender.service.GenderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    @Mock
    private IGenderRepository genderRepository;

    @Mock
    private EmailHtmlSender emailHtmlSender;

    @InjectMocks
    private EmailService emailService;

    @Test
    void generateBase64URL() {
        final Gender gender = new Gender(0, "","ElDorado","Jean-Pierre","G", LocalDateTime.now(),true);
        assertEquals("MjI7RWxEb3JhZG87SmVhbi1QaWVycmU7", this.emailService.generateBase64URL(gender));
    }
}