package fr.drkchpr.babyprojectapi.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class ObfuscatorUtilsTest {
    public static final String REGEX_MATCH_STAR = "^.*[\\*].*$";

    @Test
    void obfuscatingEmail_should_obfuscating_email(){
        Assertions.assertTrue(Pattern.matches(REGEX_MATCH_STAR, ObfuscatorUtils.obfuscatingEmail("test@test.fr")));
    }

    @Test
    void obfuscatingEmail_should_not_obfuscating_str(){
        Assertions.assertTrue("Phrase normale".equals(ObfuscatorUtils.obfuscatingEmail("Phrase normale")));
    }

    @Test
    void obfuscatingString_should_obfuscating_str(){
        Assertions.assertTrue("P***".equals(ObfuscatorUtils.obfuscatingString("Phrase normale")));
    }
}