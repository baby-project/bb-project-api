# Baby Project API

Ceci est un projet personnel réalisé pour l'apprentisage de Spring Boot (web, data, security...).  

Le but est d'avoir une API sécurisée qui permet de recueillir et degérer les votes des participants (famille, amis, collègues..) dans le  cadre de l'arrivée de notre bébé.

## V0.3.1 Corrections bugs

Modification des règles du CORS pour accepter les requêtes depuis le front.

Suppression des espaces autour des propositons de prénom des participants.

## V0.3.0 Settings, Names, Mails pt 1 & Stats OBSOLETE

**A partir de cette version, le serveur tomcat est celui embarqué par SpringBoot.**  

Création du CRUD pour le vote du prénom :
  * Creation par l'utilisateur
  * Visualisation pour les utilisateurs identifiés
  * Modification du status du vote par les utilisateurs identifiés
  * Suppression par les admin
  
Gestion des settings de l'application. Il n'est pas possible de créer des paramètres via l'API, ni d'en supprimer. 
La seule action est la mise à jour de la valeur, du fait de révéler l'information ou les deux.  
Il existe un cheminement obligatoire dans le comportement de l'application,
il faut dabord révéler le sexe pour ensuite révélé le prénom.

La gestion des mails est ok pour la partie gender.

Des statistiques sont disponibles pour les votes des prénoms à l'uri : /rest/names/stats

## V0.2.1 TU
Ajout de tests unitaires automatisés sur la partie Gender. (il était temps ^^)

## V0.2.0 DashBoard Parents

Ajout de Spring Security (BASIC AUTH) avec stratégie de blacklist.  

Création du "CRUD" sur les votes Gender :
  * Creation par l'utilisateur
  * Visualisation pour les utilisateurs identifiés
  * Modification du status du vote par les utilisateurs identifiés
  * Suppression par les admin
 

La modification du contenu (nom, prenom, choix et date) d'un vote n'est pas possible.  
Le contenu sensible, nom et email, sont obfusqués lors de l'affichage pour les utilisateurs non administrateurs.  

Création de la ressource Parent, qui représente un utilisateur authentifié.  
Gestion d'un rôle par parent.


## V0.1.0 Gender

Cette première version permet aux utilisateurs de parier (sans mise et gain) sur le sexe du bébé.
L'API ne permet que l'ajout de nouvelles données, elle est publique.

## A venir

  * Les mails pour les résultats du prénom
  * Documentation de l'API avec swagger
  * Divers améliorations sonar
  * Passage sous docker

